import { useState } from "react";
import { useContext } from "react";
import { createContext } from "react";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { StorageRead } from "../utils/storage";

const UserContext = createContext();

export const useUser = () => {
  return useContext(UserContext); //exposing value/context. this will return the user and setUser
};

//manages state
const UserProvider = ({ children }) => {
  //magic string
  const [user, setUser] = useState(StorageRead(STORAGE_KEY_USER)); // set to null because there's no user when app starts

  const state = {
    user,
    setUser,
  };
  return <UserContext.Provider value={state}>{children}</UserContext.Provider>;
};
export default UserProvider;
