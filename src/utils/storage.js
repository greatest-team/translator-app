export const StorageSave = (key, Value) => {
  sessionStorage.setItem(key, JSON.stringify(Value));
};

//Basically read and write from storage function

export const StorageRead = (key) => {
  const data = sessionStorage.getItem(key);
  if (data) {
    //If data exist then return JSON.parse (inverse of stringify)
    return JSON.parse(data); //so now this takes a JSON.String and converts it to a JS object
  }

  return null;
};
export const storageDelete = (key) => {
  sessionStorage.removeItem(key);
};
