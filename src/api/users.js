import { createHeaders } from "./index";
const apiUrl = process.env.REACT_APP_API_URL;
//username as argument to check if the username exists or not. The fetch apiUrl is looking for the username given

export const checkForUser = async (username) => {
  //username is argument
  try {
    const response = await fetch(`${apiUrl}?username=${username}`); //first username is key and second username is the value
    if (!response.ok) {
      throw new Error("Could not complete request");
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};

//The createUser function needs authentication before we can create resources.

export const createUser = async (username) => {
  try {
    const response = await fetch(apiUrl, {
      method: "POST", //when we post to an API, then we telling the server we want to create a new resource. And in this case, that new resource is USer
      headers: createHeaders(), //returns the new object
      body: JSON.stringify({
        username,
        translations: [],
      }), // must turn into a string because we cant send an object with a fetch request
    });
    if (!response.ok) {
      throw new Error("Could not create user" + username);
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};

// if userLength is not 0 then we should return the user

export const loginUser = async (username) => {
  const [checkError, user] = await checkForUser(username);
  //const [createError, newUser] = createUser(username);

  if (checkError !== null) {
    return [checkError, null];
  }
  if (user.length > 0) {
    return [null, user.pop()]; // user.pop because we don't to return an array, just one user.
  }
  return await createUser(username);
};


