const apiKey = process.env.REACT_APP_API_KEY;

export const createHeaders = () => {
  return {
    "Content-Type": "application/json", //content type is in quotes because JS doesn't allow you to use a dash in a name
    "x-api-key": apiKey,
  };
};
