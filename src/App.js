import "./App.css";
import Login from "./views/Login";
import Profile from "./views/Profile";
import Translator from "./views/Translator";
import { BrowserRouter, Routes, Route } from "react-router-dom";
function App() {
  return (
    <div className="App">
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />}></Route>
          <Route path="/Profile" element={<Profile />}></Route>
          <Route path="/Translator" element={<Translator />}></Route>
        </Routes>
    </BrowserRouter>
    </div>
  );
}
//h1
export default App;
