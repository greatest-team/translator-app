import { useForm } from "react-hook-form";
import { loginUser } from "../../api/users";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useState, useEffect } from "react";
import { useUser } from "../../context/UserContext";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
// import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { useNavigate } from 'react-router-dom'
import { StorageSave } from "../../utils/storage";
import { width } from "@mui/system";

const userConfig = {
    required: true,
    minLength: 3,
};
const LoginForm = () => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();

    // local state
    const { user, setUser } = useUser()
    const navigate = useNavigate()
    const [loading, setLoading] = useState(false);
    const [apiError, setApiError] = useState(null);

    //SideEffects
    useEffect(() => {
        if (user !== null) {
            navigate('/Translator')
            console.log('User has changed', user)
        }
    }, [user, navigate])

    //Event handlers
    const onSubmit = async ({ username }) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username); //we should get back a string on the first one or null if nothing went wrong and we should get null if something did go wrong or an object which represents user
        // because we are using await we don't have to put it in a fetch
        if (error !== null) {
            setApiError(error);
        }
        if (userResponse !== null) {
            StorageSave(STORAGE_KEY_USER, userResponse); //if successfully logged in, then save to local storage
            setUser(userResponse)
        }
        setLoading(false);
    };
    console.log(errors);

    const errorMessage = (() => {
        if (!errors.username) {
            return null;
        }
        if (errors.username.type === "required") {
            return <span>Username is required</span>;
        }
        if (errors.username.type === "minLength") {
            return <span>Username is too short (min. 3) </span>;
        }
    })();

    const textStyle = {
        textAlign: "center",
        marginTop: "20%",
        fontFamily: "Times New Roman",
        color: "#2db2d5"
    };
    const infoTextStyle = {
        textAlign: "center",
        width: "50%",
        marginLeft: "25%",
        marginBottom: "5%",
        fontFamily: "Times New Roman",
        color: "#DCDCDC"
    };


    const buttonStyle = {
        flexGrow: 1,
        padding: "10px",
        width: "50%",
        backgroundColor: "#2db2d5",
        marginTop: "20px",
        marginLeft: "25%",
        borderRadius: "25px",
        border: "#2db2d5",
        cursor: "pointer",
        color: "white"
    };

    const sideLeft = {
        backgroundImage: "url(/images/abstract.jpg)",
        height: "103vh",
        width: "100vw"

    };
    const sideRight = {
        backgroundColor: "white",
        height: "103vh",
        width: "100vw"
    };
    const welcomeTextStyle = {
        color: "white",
        fontSize: "50px",
        textAlign: "center",
        marginTop: "25%",
        fontFamily: "Times New Roman",
    };
    const welcomeInfoTextStyle = {
        color: "white",
        margin: "0 auto",
        fontFamily: "Times New Roman",
        width: "50%"
    };
    const imageStyle = {
        textAlign: "center",
        width: "15%",
        height: "auto",
        marginTop: "10%"
    }


    return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                    <Grid style={sideLeft} item xs={6} md={6}>
                        <h1 style={welcomeTextStyle}>WELCOME BACK</h1>
                        <p style={welcomeInfoTextStyle}>Welcome to the translator app. This app's main purpose is to translate words to sign language.
                            You go to the translation page to translate a word. Come and have fun with us!
                        </p>
                        <div className="image-container">
                            <img src="images/a.png" alt="a text" style={imageStyle} />
                            <img src="images/b.png" alt="a text" style={imageStyle} />
                            <img src="images/c.png" alt="a text" style={imageStyle} />
                        </div>
                    </Grid>
                    <Grid style={sideRight} item xs={6} md={6}>
                        <h2 style={textStyle}>Enter your username</h2>
                        <h5 style={infoTextStyle}>You must enter your username to sign in. If you have not signed in before, a new
                            user will be registered with your username.
                        </h5>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <fieldset style={{
                                width: "50%", margin: "0 auto", border: "none", backgroundColor: "#F0F0F0"
                            }}>
                                <input
                                    type="text"
                                    placeholder="Username"
                                    style={{ border: "none", backgroundColor: "#F0F0F0", fontSize: "15px", }}
                                    {...register("username", userConfig)}
                                />
                                {errorMessage}
                            </fieldset>
                            <button
                                style={buttonStyle}
                                type="submit"
                                disabled={loading}
                            >
                                Save username
                            </button>
                            {loading && <p>Logging in...</p>}
                            {apiError && <p> {apiError}</p>}
                        </form>
                    </Grid>
                </Grid>
            </Box>
        </>
    );
};
export default LoginForm;
