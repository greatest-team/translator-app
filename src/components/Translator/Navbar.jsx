import { Link } from "react-router-dom";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../context/UserContext";
import { storageDelete } from "../../utils/storage";

const Navbar = () => {
    const { setUser } = useUser(); // get access to the current user

    const handleLogoutCLick = () => {
        if (window.confirm("Are you sure?")) {
            storageDelete(STORAGE_KEY_USER); //calling the StorageDelete function created in storage
            setUser(null);
        }
    };
    const linkStyle = {
        margin: "1rem",
        textDecoration: "none",
        color: '#E5EAE4',
        padding: "50px"
    };
    const buttonStyle = {
        padding: "10px",
        backgroundColor: "#2db2d5",
        cursor: "pointer",
        color: "#E5EAE4",
        border: "none"
    }
    const headerStyle = {
        flexGrow: 1,
        padding: "15px",
        marginBottom: "5%",
        backgroundColor: "#2db2d5"
    }


    return (
        <div style={headerStyle}>
            <div>
                <Link to="/Profile" style={linkStyle}> Profile </Link>
                <Link to="/Translator" style={linkStyle}> Translator </Link>
                <button onClick={handleLogoutCLick} style={buttonStyle} >Logout </button>
            </div>
        </div>
    );
};
export default Navbar;