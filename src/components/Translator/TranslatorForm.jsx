import { useForm } from "react-hook-form";
import Navbar from "./Navbar";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";

const TranslatorForm = ({ onTranslation }) => {
  // const [totalArr, setTotalArr] = useState();
  const { register, handleSubmit } = useForm();

  const onSubmit = ({ translationNotes }) => {
    onTranslation(translationNotes);
  };

  const btnPrimary = {
    backgroundColor: "#2db2d5",
    color: "white",
    border: "none",
    padding: "5px",
  };

  return (
    <>
      <Navbar />
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item xs={4} md={4}></Grid>
          <Grid item xs={4} md={4}>
            <div>
              <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset style={{ borderRadius: "25px", margin: "0 auto", backgroundColor: "#F0F0F0" }}>
                  <input
                    type="text"
                    placeholder="Translate your word here"
                    style={{ border: "none", backgroundColor: "#F0F0F0", fontSize: "15px", }}
                    {...register("translationNotes")}
                  />

                  <button className="btn" style={btnPrimary} onClick={handleSubmit}>
                    Translate word!
                  </button>
                </fieldset>
              </form>
            </div>
          </Grid>
          <Grid item xs={4} md={4}></Grid>
        </Grid>
      </Box>
    </>
  );
};

export default TranslatorForm;
