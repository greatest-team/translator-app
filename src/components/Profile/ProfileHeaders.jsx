import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";

const ProfileHeaders = ({ username }) => {

  const headerStyle = {
    fontSize: "35px",
    fontFamily: "Times New Roman",
    color: "#2db2d5",
    marginLeft: "15%",
    marginBottom: "0"
  }
  const translationHistoryStyle = {
    fontSize: "25px",
    fontFamily: "Times New Roman",
    marginLeft: "25%",
    color: "#2db2d5"
  }
  const styling = {
    borderTop: "2px"
  }
  return (
    <header style={styling}>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item xs={3} md={3}></Grid>
          <Grid item xs={6} md={6}>
            <h4 style={headerStyle}>Hello, Welcome back {username}</h4>
            <h4 id="translation-history" style={translationHistoryStyle}>Your Translation history:</h4>
          </Grid>
        </Grid>
      </Box>
    </header>
  );
};
export default ProfileHeaders;
