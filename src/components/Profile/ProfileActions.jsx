import { translationClearHistory } from "../../api/translations";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../context/UserContext";
import { StorageSave } from "../../utils/storage";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";



const ProfileActions = ({ username }) => {
  const { user, setUser } = useUser(); // get access to the current user

  //Clears history once the clear history button is clicked and  this function updates the translation history 
  const ClearHistoryClick = async () => {
    if (!window.confirm("Are you sure you want to DELETE YOUR HISTORY??/\nThis can not be undone!")) {
      return;
    }

    const [clearError] = await translationClearHistory(user.id);

    if (clearError !== null) {
      return;
    }
    //updating user and translation
    const updatedUser = {
      ...user,
      translations: [],
    };

    StorageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);
  };

  const buttonStyle = {
    padding: "12px",
    backgroundColor: "#2db2d5",
    borderRadius: "10px",
    border: "none",
    cursor: "pointer",
    marginLeft: "25%",
    color: "white"
  };

  return (

    <div>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item xs={4} md={4}>
          </Grid>
          <Grid item xs={4} md={4}>
            <button onClick={ClearHistoryClick} style={buttonStyle}>Clear history</button>
          </Grid>
          <Grid item xs={4} md={4}></Grid>
        </Grid>
      </Box>
    </div>
  );
};
export default ProfileActions;
