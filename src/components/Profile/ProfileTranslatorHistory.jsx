import TranslatorHistoryItem from "./TranslatorHistoryItem";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";

const ProfileTranslatorHistory = ({ translations }) => {
  // basically if its more than 10 items or not then it sorts the translations into the right order
  // it selects the last 10 items if more than 10 translations 
  if (translations) {
    translations.length >= 10
      ? (translations = translations
        .slice(translations.length - 10, translations.length)
        .reverse())
      : (translations = translations.slice(0, translations.length).reverse())
  }
  const translationsList =
    translations.map((translation, index) =>
      <TranslatorHistoryItem key={index + "-" + translation} translation={translation} />
    );
  return (
    <section>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item xs={4} md={4}></Grid>
          <Grid item xs={4} md={4}>
            <ul>{translationsList}</ul>
          </Grid>
          <Grid item xs={4} md={4}></Grid>
        </Grid>
      </Box>
    </section>
  );
};
export default ProfileTranslatorHistory;