
const TranslatorHistoryItem = ({ translation }) => {
  const translationWordStyle = {
    color: "#2db2d5",
    marginLeft: "25%"
  }
  return (<li style={translationWordStyle}>{translation}</li>)
};
export default TranslatorHistoryItem;

