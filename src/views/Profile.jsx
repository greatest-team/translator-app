import ProfileActions from "../components/Profile/ProfileActions";
import ProfileHeaders from "../components/Profile/ProfileHeaders";
import ProfileTranslatorHistory from "../components/Profile/ProfileTranslatorHistory";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import Navbar from "../components/Translator/Navbar";

const Profile = () => {
  const { user } = useUser(); // get access to the current user

  return (
    <>
      <Navbar />
      <ProfileHeaders username={user.username} />
      <ProfileTranslatorHistory translations={user.translations} />
      <ProfileActions />
    </>
  );
};
export default withAuth(Profile);
