import { useState } from "react";
import { translationAdd } from "../api/translations";
import TranslatorForm from "../components/Translator/TranslatorForm";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { StorageSave } from "../utils/storage";

const Translator = () => {
  let sign = [];
  let image = [];

  //user
  
  const [input, setInput] = useState();
  const { user, setUser } = useUser();

  

  //firstly this function takes the text from user and makes/reads every word as lowercase. And takes every single letter comparing it with the image.png and gives result with those that matches
  const translator = (text) => {
    sign = text.toLowerCase().split("");
    image = sign.map((x) => "images/" + x + ".png");
    setInput(image);
    console.log(image);
  };

  //handles the word and removes whitespace
  const translating = async (notes) => {
    const translation = notes.trim();
    translator(notes);
    const [error, updatedUser] = await translationAdd(user, translation);
    if (error !== null) {
    }
    StorageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);
  };


  return (
    <>
      <TranslatorForm onTranslation={translating} />
      {input && input.map((x) => <img src={x} alt={sign} />)}
    </>
  );
};
export default withAuth(Translator);
